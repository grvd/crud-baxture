# Getting Started
# This project contains simple CRUD API as given in the assignment

# On technical front we have used NodeJS with hapi.js a open-source framework for developing scalable web applications, API servers, websites, and HTTP proxy applications with hapi.js
# For schema builder we have used KnexJS, ObjectionJS an SQL-friendly ORM for Node.js
# For request validations we have used Joi the most powerful schema description languageand data validator for JavaScript

# We have used postgresql DB for data storage.

# Pre-requisite
1) Install node and npm (https://nodejs.org/en/)
2) Postgres should be installed on your machine with user name "postgres" and password any thing of your choice.
3) Make sure to create an empty DB
4) Run "npm i" on terminal to install all packages
5) Later run the schema migration using "knex migrate:latest"
6) Then you can start server by running "npm run dev"/"node index.js" command.
7) Once server starts, you can test various APIs on "http://localhost:4000/api/users"