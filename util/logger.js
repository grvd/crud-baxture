const pino = require('pino')({
    redact: ['hostname', 'pid', 'req.headers', 'res.headers'],
  });
  
  module.exports = pino;
  