const knex = require('knex');
const { Model } = require('objection');
const knexConfig = require('../knexfile');

const env = process.env.NODE_ENV || 'development';
const envConfig = knexConfig[env];

module.exports = Model.knex(knex(envConfig));
