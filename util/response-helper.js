class ResponseHelper {
  static success(message = null, data, code = 200) {
    return {
      success: true, 
      message, 
      data,
      code,
    };
  }
}

module.exports = ResponseHelper;