exports.up = (knex) => Promise.all([
    knex.schema.createTable('users', (table) => {
      table.increments('id').primary();
      table.string('username', 50).notNullable();
      table.integer('age').notNullable();
      table.specificType('hobbies', 'text []');
      table.string('uuid', 255);
      table.timestamps(true, true);
    }),
  ]);
  
exports.down = (knex) => Promise.all([knex.schema.dropTableIfExists('users')]);
  