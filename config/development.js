module.exports = {
    server: {
      port: 4000,
    },
    database: {
      client: 'pg',
      connection: {
        database: 'CRUD-Baxture',
        user: 'postgres',
        password: 'root',
        host: 'localhost',
        // port: 25060,
        // ssl: true,
      },
      pool: {
        min: Number(process.env.DATABASE_POOL_MIN || 0),
        max: Number(process.env.DATABASE_POOL_MAX || 5),
      },
    },
    api_version: '1.0',
  };
  