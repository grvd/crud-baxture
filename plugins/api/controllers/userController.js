const knex = require("knex");
const UserModel = require("../../../models/Users");

const userSelectFields = ["id", "username", "age", "hobbies", "uuid"];

class UserController {
  // Get all user query
  static getUsers() {
    return UserModel.query().select(userSelectFields).orderBy('id');
  }

   // Query to fetch user by id
  static getUserById(userId) {
    return UserModel.query().select(userSelectFields).where("id", userId);
  }

  // Query to create new user
  static createUser(payload) {
    return UserModel.query().insert({
      uuid: payload.userUuid,
      username: payload.username,
      age: payload.age,
      hobbies: payload.hobbies,
    });
  }

  // Update user data by id
  static updateUser(userId, payload) {
    return UserModel.query()
      .where('id', userId)
      .update({
        username: payload.username,
        age: payload.age,
        hobbies: payload.hobbies,
        updated_at: new Date(),
      })
    .returning('*');
  }

  // Delete a specific user by its id
  static deleteUser(userId) {
    return UserModel.query()
      .deleteById(userId)
      .returning('*');
  }
}

module.exports = UserController;
