const Boom = require("@hapi/boom");
const Joi = require("joi");
const UserService = require("../services/userService.js");
const RequestValidationError = require("../../../util/request-val-error");
const r = require('../../../util/response-helper.js');

/**
 * API to get all users info
 */
const getAllUsers = {
  method: "GET",
  path: "/users",
  options: {
    handler: async (request) => {
      try {
        const fetchedUser = await UserService.getUsers();
        if (!fetchedUser) {
          return Boom.notFound(
            "No users found."
          );
        }
        return r.success('Users info fetched succesfully.', fetchedUser, 200);
      } catch (err) {
        request.log(["error", "get-all-users"], err);
        if (err instanceof RequestValidationError) {
          return {
            success: false,
            message: err.message,
            data: null,
          };
        }
        return Boom.internal();
      }
    },
  },
};

/**
 * API to get user info by id
 */
const getUserById = {
  method: "GET",
  path: "/users/{userId}",
  options: {
    validate: {
      params: Joi.object().keys({
        userId: Joi.number().required().label("User Id"),
      }),
    },
    handler: async (request) => {
      try {
        const fetchedUser = await UserService.getUserById(request.params);
        if (fetchedUser.length === 0) {
          return Boom.notFound(
            "User not found. Please make sure you have given correct user id."
          );
        }
        return r.success('User info fetched succesfully.', fetchedUser, 200);
      } catch (err) {
        request.log(["error", "get-user-by-id"], err);
        if (err instanceof RequestValidationError) {
          return {
            success: false,
            message: err.message,
            data: null,
          };
        }
        return Boom.internal();
      }
    },
  },
};

/**
 * API to create new user
 */
const createUser = {
  method: "POST",
  path: "/users",
  options: {
    validate: {
      payload: Joi.object().keys({
        username: Joi.string().required().label('User Name').max(50),
        age: Joi.number().required().label('Age'),
        hobbies: Joi.array().required().label('Hobbies')
      }),
    },
    handler: async (request) => {
      try {
        const newUser = await UserService.createUser(request.payload);
        if (newUser) {
          return r.success('User created succesfully.', newUser, 201);
        }
      } catch (err) {
        request.log(["error", "create-user"], err);
        if (err instanceof RequestValidationError) {
          return {
            success: false,
            message: err.message,
            data: null,
          };
        }
        return Boom.internal();
      }
    },
  },
};

/**
 * API to update user info
 */
const updateUser = {
  method: "PUT",
  path: "/users/{userId}",
  options: {
    validate: {
      params: Joi.object().keys({
        userId: Joi.number().required().label("User Id"),
      }),
      payload: Joi.object().keys({
        username: Joi.string().required().label('User Name').max(50),
        age: Joi.number().required().label('Age').min(1).max(100),
        hobbies: Joi.array().required().label('Hobbies')
      }),
    },
    handler: async (request) => {
      try {
        const updatedUser = await UserService.updateUser(request.params.userId, request.payload);
        if (updatedUser.length === 0) {
          return Boom.notFound(
            "User not found. Please make sure you have given correct user id."
          );
        }
        return r.success('User info updated succesfully.', updatedUser, 200);
      } catch (err) {
        request.log(["error", "update-user"], err);
        if (err instanceof RequestValidationError) {
          return {
            success: false,
            message: err.message,
            data: null,
          };
        }
        return Boom.internal();
      }
    },
  },
};

/**
 * API to update user info
 */
const deleteUser = {
  method: "DELETE",
  path: "/users/{userId}",
  options: {
    validate: {
      params: Joi.object().keys({
        userId: Joi.number().required().label("User Id"),
      }),
    },
    handler: async (request) => {
      try {
        const deletedUser = await UserService.deleteUser(request.params);
        if (!deletedUser) {
          return Boom.notFound(
            "User not found. Please make sure you have given correct user id."
          );
        }
        return r.success(`User with Id ${request.params.userId} deleted.`, deletedUser, 200);
      } catch (err) {
        request.log(["error", "delete-user"], err);
        if (err instanceof RequestValidationError) {
          return {
            success: false,
            message: err.message,
            data: null,
          };
        }
        return Boom.internal();
      }
    },
  },
};

module.exports = [getUserById, createUser, updateUser, getAllUsers, deleteUser];