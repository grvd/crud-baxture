const UserController = require('../controllers/userController');
const RequestValidationError = require("../../../util/request-val-error");
const { v4: uuidv4 } = require('uuid');
class UserService {
  // Function to validate input/payload
  static async validatePayload(payload){
    if (payload.username) {
      if (payload.username.length > 50) throw new RequestValidationError('User Name cannot be greater than 50 characters.')
    }
    if (payload.userId) {
      if (payload.userId < 0) throw new RequestValidationError('User id can not be negative.');
    }

    if (payload.age === 0 || payload.age > 100) throw new RequestValidationError('Age cannot be less than 1 or greater than 100.');
    
    if (payload.hobbies) {
      if (!Array.isArray(payload.hobbies)) throw new RequestValidationError('Hobbies must be an array of strings.')
    }
    return true;
  }

  // Function to fetch all users
  static async getUsers() {
    const users = await UserController.getUsers();
    return users;
  }

  // Function to fetch user by id
  static async getUserById(payload) {
    await this.validatePayload(payload);
    const userInfo = await UserController.getUserById(payload.userId);
    return userInfo;
  }

  // Function to create user based username, age & hobbies
  static async createUser(payload) {
    await this.validatePayload(payload);
    payload.userUuid = uuidv4();
    const newUser = await UserController.createUser(payload);
    return newUser;
  }

  // Function to update user by details
  static async updateUser(userId, payload) {
    await this.validatePayload(payload);
    const updatedUser = await UserController.updateUser(userId, payload);
    return updatedUser;
  }

  // Function to delete user
  static async deleteUser(payload) {
    await this.validatePayload(payload);
    const deleteUser = await UserController.deleteUser(payload.userId);
    return deleteUser;
  }
}

module.exports = UserService;