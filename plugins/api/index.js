const Path = require('path');
const HapiAutoRoutes = require('hapi-auto-routes');

const APIPlugin = {
  name: 'version/api',
  async register(server) {
    HapiAutoRoutes.bind(server).register({
      pattern: Path.join(__dirname, 'routes/**/*.js'),
    });
  },
};

module.exports = APIPlugin;
