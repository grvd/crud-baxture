const Hapi = require('@hapi/hapi')
const Config = require('config');
const HapiPino = require("hapi-pino");
const Boom = require("@hapi/boom");
const Logger = require("./util/logger");
const Plugin = require("./plugins/api");
require('./util/db')

const server = Hapi.server({
  port: Config.server.port,
  routes: {
    validate: {
      failAction: async (req, h, err) =>
        Boom.badRequest(err.details[0].message),
    },
    state: {
      parse: false,
      failAction: "ignore",
    },
  },
});

async function startServer() {
  try {
    await server.register({
      plugin: HapiPino,
      options: {
        instance: Logger,
      },
    });
    server.ext({
      type: "onRequest",
      method: (request, h) => {
        if (request.path === "health") {
          return h.continue;
        }
        return h.continue;
      },
    });
    if (Config.database.connection.host) {
      await server.start();
      server.log(["system", "info"], `Server started ${server.info.uri}`);
    } else {
      server.log(
        ["system", "error"],
        "Database host is not set. Cannot start service"
      );
      throw new Error("Database host is not set. Cannot start service.");
    }
    await server.register({
      plugin: Plugin,
      routes: {
        prefix: "/api",
      },
    });
  } catch (error) {
    server.log(["system", "error"], error);
  }
}

startServer();

process.on("SIGINT", () => {
  server.log(["system", "info"], "stopping server...");
  server.stop({ timeout: 10000 }).then((err) => {
    server.log(["system", "info"], "server stopped");
    process.exit(err ? 1 : 0);
  });
});
