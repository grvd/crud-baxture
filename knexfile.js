const config = require('config');
const env = process.env.NODE_ENV || 'development';

if (config.database.connection.ssl) {
  config.database.connection.ssl = {
    rejectUnauthorized: false,
  };
}

module.exports = {
  development: {
    client: config.database.client,
    connection: config.database.connection,
    database: config.database.connection.database,
    password: config.database.connection.password,
    pool: config.database.pool,
    debug: process.env.DEBUG_KNEX,
    migrations: {
      tableName: 'knex_migrations',
    },
  },
  staging: {
    client: config.database.client,
    connection: config.database.connection,
    pool: config.database.pool,
    debug: process.env.DEBUG_KNEX,
    migrations: {
      tableName: 'knex_migrations',
    },
  },
  production: {
    client: config.database.client,
    connection: config.database.connection,
    pool: config.database.pool,
    debug: process.env.DEBUG_KNEX,
    migrations: {
      tableName: 'knex_migrations',
    },
  },
};
